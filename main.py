from apiHandler import Joomla
from secretHandler import secret
from classes.article import Article
from classes.category import Category

secret = secret()
apiKey = secret.loadSecret("secret.txt")
if apiKey[0] != 0:
    exit(1)

apiKey = apiKey[1]

joomla = Joomla(joomlaURL="https://pinguin-software.de", apiKey=apiKey)
'''
text = """
<p style="text-align: left;"><strong>Python Joomla API</strong></p>
<p style="text-align: left;"><a href="https://gitlab.com/HendrikHeine/joomla-api/-/archive/main/joomla-api-main.zip" target="_blank" rel="noopener">Download</a> | <a href="https://gitlab.com/HendrikHeine/joomla-api" target="_blank" rel="noopener">GitLab</a></p>
<p style="text-align: left;">Readme:</p>
<p style="text-align: left;"><strong>Joomla API</strong></p>
<p style="text-align: left;"><a href="https://docs.joomla.org/J4.x:Joomla_Core_APIs" target="_blank" rel="noopener">Joomla 4.x API Doc</a></p>
<p style="text-align: left;"><strong>Getting started</strong></p>
<p style="text-align: left;"><a href="https://gitlab.com/HendrikHeine/joomla-api/-/wikis/home">Wiki</a></p>
"""
'''
#joomla.createNewArticle(content=text)

#articles = joomla.getAllArticles()
#for article in articles:
#    article:Article
#    print(article.attributes.text)
#    print("------------------------------------------------------------------------")

categories = joomla.getCategories()
for category in categories:
    category:Category
    print(category.title)
    print(category.id)
    print("")