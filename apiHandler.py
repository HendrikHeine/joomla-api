import requests
import json
import dateTime
from classes.article import Article
from classes.category import Category
from classes.users import User

class Joomla:
    def __init__(self, joomlaURL:str, apiKey:str) -> None:
        self.__api = "api/index.php/v1"
        self.__apiKey = apiKey
        self.__joomlaURL = joomlaURL
        self.__joomlaAPI = f"{self.__joomlaURL}/{self.__api}"

        self.__authHead = {"X-Joomla-Token": self.__apiKey, "Content-Type": "application/json"}

    def __processResponse(self, response:requests.Response):
        if response.status_code == 200:
            return response.content.decode()
        else:
            print("ERROR")
            print(f"Code: {response.status_code}")
        
    def __get(self, url) -> requests.Response:
        return requests.api.get(url=url, headers=self.__authHead)

    def __post(self, url, data) -> requests.Response:
        response = requests.api.post(url=url, headers=self.__authHead, json=data)
        print(response.content.decode())
        return response

    def __delete(self, url) -> requests.Response:
        return requests.api.delete(url=url, headers=self.__authHead)

    def __patch(self, url) -> requests.Response:
        return requests.api.patch(url=url, headers=self.__authHead)

    
    # Content
    def getAllArticles(self) -> list:
        response = self.__processResponse(response=self.__get(url=f"{self.__joomlaAPI}/content/articles"))
        response = json.loads(response)
        articles = []
        for article in response['data']:
            articleObj = Article(jsonData=article)
            print(f"Type: {articleObj.type}")
            print(f"ID: {articleObj.id}")
            print(f"Title: {articleObj.attributes.title}")
            articles.append(articleObj)
            print("")
        return articles

    def createNewArticle(self, content:str, title:str, alias=None):
        date = dateTime.get()

        data = {
            "alias": title.replace(" ", "-"),
            "title": title,
            "articletext": content,
            "catid": 17,
            "language": "*",
            "metadesc": "",
            "metakey": "",
            "access": 1,
            "state": 1,
            "publish_up": f"·{date[0]} {date[1]}"
        }
        response = self.__processResponse(response=self.__post(url=f"{self.__joomlaAPI}/content/articles", data=data))
        print(response)

    def getCategories(self):
        response = self.__processResponse(response=self.__get(url=f"{self.__joomlaAPI}/content/categories"))
        categories = json.loads(response)
        categoryList = []
        for category in categories['data']:
            categoryList.append(Category(jsonData=category))
        #print(json.dumps(categories, indent=4))
        return categoryList
        

    # User
    def getAllUsers(self) -> list:
        response = self.__processResponse(response=self.__get(url=f"{self.__joomlaAPI}/users"))
        response = json.loads(response)
        users = []
        for user in response['data']:
            userObj = User(jsonData=user)
            users.append(userObj)
            print(f"Type: {userObj.type}")
            print(f"ID: {userObj.id}")
            print(f"Name: {userObj.attributes.name}")
            print("")
        return users
