class User:
    def __init__(self, jsonData:dict) -> None:
        self.type = jsonData['type']
        self.id = jsonData['id']
        self.attributes = Attributes(jsonData=jsonData['attributes']) 


class Attributes:
    def __init__(self, jsonData:dict) -> None:
        self.id = jsonData['id']
        self.name = jsonData['name']
        self.username = jsonData['username']
        self.email = jsonData['email']
        self.block = jsonData['block']
        self.sendEmail = jsonData['sendEmail']
        self.registerDate = jsonData['registerDate']
        self.lastvisitDate = jsonData['lastvisitDate']
        self.lastResetTime = jsonData['lastResetTime']
        self.resetCount = jsonData['resetCount']
        self.group_count = jsonData['group_count']
        self.group_names = jsonData['group_names']