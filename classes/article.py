class Article:
    '''Class for Article'''
    def __init__(self, jsonData:dict) -> None:
        self.type = jsonData['type']
        self.id = jsonData['id']
        self.attributes = Attributes(jsonData=jsonData['attributes'])
        #self.relationships = Relationships(jsonData=jsonData['relationships'])


class Attributes:
    def __init__(self, jsonData) -> None:
        self.id = jsonData['id']
        #self.asset_id = jsonData['asset_id']
        self.title = jsonData['title']
        self.alias = jsonData['alias']
        self.state = jsonData['state']
        self.access = jsonData['access']
        #self.created = jsonData['created']
        #self.created_by = jsonData['created_by']
        #self.created_by_alias = jsonData['created_by_alias']
        #self.modified = jsonData['modified']
        #self.featured = jsonData['featured']
        #self.language = jsonData['language']
        #self.hits = jsonData['hits']
        #self.publish_up = jsonData['publish_up']
        #self.publish_down = jsonData['publish_down']
        #self.note = jsonData['note']
        #self.images = Images(jsonData=jsonData['images'])
        self.metakey = jsonData['metakey']
        self.metadesc = jsonData['metadesc']
        #self.metadata = Metadata(jsonData=jsonData['metadata'])
        #self.version = jsonData['version']
        #self.featured_up = jsonData['featured_up']
        #self.featured_down = jsonData['featured_down']
        #self.typeAlias = jsonData['typeAlias']
        self.text = jsonData['text']
        #self.about_the_author = jsonData['about-the-author']
        #self.tags = jsonData['tags']


class Relationships:
    def __init__(self, jsonData:dict) -> None:
        self.category = Category(jsonData=jsonData['category'])
        self.created_by = Created_by(jsonData=jsonData['created_by'])


class Images:
    def __init__(self, jsonData:dict) -> None:
        self.image_intro = jsonData['image_intro']
        self.image_intro_alt = jsonData['image_intro_alt']
        self.float_intro = jsonData['float_intro']
        self.image_intro_caption = jsonData['image_intro_caption']
        self.image_fulltext = jsonData['image_fulltext']
        self.image_fulltext_alt = jsonData['image_fulltext_alt']
        self.float_fulltext = jsonData['float_fulltext']
        self.image_fulltext_caption = jsonData['image_fulltext_caption']

class Metadata:
    def __init__(self, jsonData:dict) -> None:
        self.robots = jsonData['robots']
        self.author = jsonData['author']
        self.rights = jsonData['rights']


class Category:
     def __init__(self, jsonData:dict) -> None:
        self.data = Data(jsonData=jsonData['data'])


class Created_by:
    def __init__(self, jsonData:dict) -> None:
        self.data = Data(jsonData=jsonData['data'])


class Data:
    def __init__(self, jsonData:dict) -> None:
        self.type = jsonData['type']
        self.id = jsonData['id']
