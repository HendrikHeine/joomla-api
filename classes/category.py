class Category:
    def __init__(self, jsonData) -> None:
        self.id = jsonData['id']
        self.title = jsonData['attributes']['title']
        self.alias = jsonData['attributes']['alias']